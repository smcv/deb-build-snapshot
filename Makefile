# Copyright 2018-2020 Collabora Ltd.
# SPDX-License-Identifier: MIT

PYTHON ?= python3

check: lint unit-test
.PHONY: check

lint:
	prove -v tests/*.sh
.PHONY: lint

unit-test:
	prove -e$(PYTHON) -v tests/*.py
.PHONY: unit-test

coverage:
	$(PYTHON) -m coverage erase
	COVERAGE_FILE=$(CURDIR)/.coverage prove -e$(PYTHON) -v tests/*.py
	$(PYTHON) -m coverage report -m
	$(PYTHON) -m coverage html

dist:
	./build-vendored.sh
