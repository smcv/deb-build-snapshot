PYTHON ?= python3

check:
	prove -v tests/*.sh
	prove -e$(PYTHON) -v tests/*.py

coverage:
	$(PYTHON) -m coverage erase
	COVERAGE_FILE=$(CURDIR)/.coverage prove -e$(PYTHON) -v tests/*.py
	$(PYTHON) -m coverage report -m
	$(PYTHON) -m coverage html

dist:
	./build-standalone.sh
