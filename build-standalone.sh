#!/bin/sh

# Copyright © 2019 Collabora Ltd.
#
# SPDX-License-Identifier: MIT
#
# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
# CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
# SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

set -eu

me="$0"
me="${me##*/}"

PACKAGE=deb-build-snapshot-standalone
VERSION=
apt=
suffix=

usage () {
    if [ "$1" -ne 0 ]; then
        exec >&2
    fi

    echo "Usage: $me [OPTIONS]"
    echo
    echo "Build a standalone version of deb-build-snapshot."
    echo
    echo "Options:"
    echo "--apt                         Get dependencies from apt."
    echo "--no-apt                      Get dependencies from git [default]."
    echo "--set-suffix=SUFFIX           Add SUFFIX to the filename."
    echo "--set-version=VERSION         Mark it as version VERSION."
    exit "$1"
}

getopt_temp="help"
getopt_temp="${getopt_temp},apt"
getopt_temp="${getopt_temp},no-apt"
getopt_temp="${getopt_temp},set-suffix:"
getopt_temp="${getopt_temp},set-version:"

getopt_temp="$(getopt -o '' --long "$getopt_temp" -n "$me" -- "$@")"
eval "set -- $getopt_temp"
unset getopt_temp

while [ "$#" -gt 0 ]; do
    case "$1" in
        (--help)
            usage 0
            # not reached
            ;;

        (--apt)
            apt=yes
            shift
            ;;

        (--no-apt)
            apt=
            shift
            ;;

        (--set-suffix)
            suffix="$2"
            shift 2
            ;;

        (--set-version)
            VERSION="$2"
            shift 2
            ;;

        (--)
            shift
            break
            ;;

        (-*)
            log "Unknown option: $1"
            usage 64    # EX_USAGE from sysexits.h
            # not reached
            ;;

        (*)
            break
            ;;
    esac
done

if [ "$#" -gt 0 ]; then
    usage 64
fi

if [ -n "${suffix}" ]; then
    PACKAGE="${PACKAGE}-${suffix}"
fi

if [ -z "${VERSION}" ]; then
    VERSION="$(git describe --match 'v*')"
    VERSION="${VERSION#v}"
fi

tempdir="$(mktemp -d)"
builddir="${tempdir}/${PACKAGE}-${VERSION}"

cleanup () {
    rm -fr "${tempdir}"
}
trap cleanup EXIT

mkdir "${builddir}"
mkdir "${builddir}/third-party"

if [ -n "$apt" ]; then
    (
        cd "${builddir}/third-party"
        apt-get source chardet python-debian six
        mv chardet-*/ chardet
        mv python-debian-*/ python-debian
        mv six-*/ six
        rm chardet_* python-debian_* six_*
    )
else
    git clone --depth=1 -b debian/3.0.4-3 \
    https://salsa.debian.org/python-team/modules/chardet.git \
    "${builddir}/third-party/chardet"
    (
        cd "${builddir}/third-party/chardet"
        git checkout 3b4640c3c9b9ff024ae59cd4ac8f9cc1cffb1f34
    )

    git clone --depth=1 -b 0.1.35 \
    https://salsa.debian.org/python-debian-team/python-debian.git \
    "${builddir}/third-party/python-debian"
    (
        cd "${builddir}/third-party/python-debian"
        git checkout b84db81cdcfdfca1931fc5aee1852eab34132651
    )

    git clone --depth=1 -b debian/1.12.0-1 \
    https://salsa.debian.org/python-team/modules/six.git \
    "${builddir}/third-party/six"
    (
        cd "${builddir}/third-party/six"
        git checkout a54b58a8c018324480ceb5bd2e371188c56214db
    )
fi

(
    cd "${builddir}"
    ln -fns third-party/chardet/chardet .
    ln -fns third-party/python-debian/lib/* .
    ln -fns third-party/six/six.py .
)

git archive \
    --format=tar \
    -o "${tempdir}/${PACKAGE}-${VERSION}.tar" \
    --prefix="${PACKAGE}-${VERSION}/" \
    HEAD

tar \
    --exclude-vcs \
    --append \
    -C "${tempdir}" \
    -f "${tempdir}/${PACKAGE}-${VERSION}.tar" \
    "${PACKAGE}-${VERSION}/"

gzip -9n "${tempdir}/${PACKAGE}-${VERSION}.tar"
mv "${tempdir}/${PACKAGE}-${VERSION}.tar.gz" .
