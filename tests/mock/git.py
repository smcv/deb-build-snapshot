#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Copyright © 2020 Collabora Ltd.
#
# SPDX-License-Identifier: MPL-2.0
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

import fnmatch
import json
import logging
import os
import sys

try:
    import typing
    typing
except ImportError:
    pass


MOCK_COMMIT = '1a2b3c4'
MOCK_COMMIT_COUNT = 123


logger = logging.getLogger('mock-git')


class MockTag:
    def __init__(
        self,
        age: int,
        tag: str,
        has_packaging_changes: bool = True,
        has_upstream_changes: bool = True,
        **kwargs
    ):
        self.age = age
        self.tag = tag
        self.has_packaging_changes = has_packaging_changes
        self.has_upstream_changes = has_upstream_changes


def main() -> None:
    logging.getLogger().setLevel(logging.INFO)
    logging.basicConfig()
    logger.info('%r', sys.argv[1:])

    mock_tags = []      # type: typing.List[MockTag]

    # Format: one or more lines each containing a JSON representation
    # of a tag
    for line in os.getenv('MOCK_GIT_TAGS', '').splitlines():
        obj = json.loads(line)
        mock_tags.append(MockTag(**obj))

    # Most-recent-first (increasing order of age)
    mock_tags.sort(key=lambda mock: mock.age)

    if sys.argv[1:5] == ['diff', '--name-only', '--no-renames', '-z']:
        assert len(sys.argv) == 6
        assert sys.argv[5].endswith('..HEAD')
        ref = sys.argv[5][:-6]

        for tag in mock_tags:
            if tag.tag == ref:
                if tag.has_packaging_changes:
                    print('debian/rules', end='\0')

                if tag.has_upstream_changes:
                    print('src/hello.c', end='\0')

                break
        else:
            raise ValueError('No such tag')

    elif sys.argv[1:4] == ['describe', '--long', '--tags']:
        assert len(sys.argv) == 5
        assert sys.argv[4].startswith('--match=')
        pattern = sys.argv[4][len('--match='):]

        for tag in mock_tags:
            if fnmatch.fnmatchcase(tag.tag, pattern):
                print('{}-{}-{}'.format(tag.tag, tag.age, MOCK_COMMIT))
                break
        else:
            raise SystemExit(1)

    elif sys.argv[1:] == ['rev-list', 'HEAD']:
        for i in range(MOCK_COMMIT_COUNT - 1):
            print('%07x' % i)

        print(MOCK_COMMIT)

    elif sys.argv[1:] == ['show', '--pretty=format:%h', '-s']:
        print(MOCK_COMMIT)

    else:
        raise ValueError('No mock implementation of {!r}'.format(
            sys.argv[1:],
        ))


if __name__ == '__main__':
    main()
