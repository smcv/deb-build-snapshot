#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Copyright © 2020 Collabora Ltd.
#
# SPDX-License-Identifier: MPL-2.0
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

import json
import os
import shlex
import shutil
import subprocess
import sys
import tempfile
import unittest
from itertools import product

try:
    import typing
    typing
except ImportError:
    pass

try:
    from tap.runner import TAPTestRunner
except ImportError:
    TAPTestRunner = None    # type: ignore


MOCK_COMMIT = '1a2b3c4'
MOCK_COMMIT_COUNT = 123


class TestVersionGen(unittest.TestCase):
    def setUp(self) -> None:
        self.mock_git = '{} {}'.format(
            shlex.quote(sys.executable),
            shlex.quote(
                os.path.join(
                    os.path.dirname(os.path.abspath(__file__)),
                    'mock',
                    'git.py',
                ),
            )
        )

        script = os.path.join(
            os.path.dirname(os.path.dirname(os.path.abspath(__file__))),
            'deb-git-version-gen',
        )

        self.deb_git_version_gen = [
            sys.executable,
            script,
            '--debug',
            '--mock-git=' + self.mock_git,
        ]

        if os.getenv('COVERAGE_FILE') is not None:
            self.deb_git_version_gen[1:1] = [
                '-m',
                'coverage',
                'run',
                '--append',
                '--branch',
                '--include=' + script,
            ]

        print('# ' + repr(self.deb_git_version_gen), file=sys.stderr)

        self.tmpdir = tempfile.TemporaryDirectory()

    def populate_tmpdir(
        self,
        format=None,                    # type: typing.Optional[str]
        native=False,
        old_versions=(),                # type: typing.Sequence[str]
        source='foo',
        version=None,                   # type: typing.Optional[str]
    ) -> None:
        if format is None:
            if native:
                format = '3.0 (native)'
            else:
                format = '3.0 (quilt)'

        if version is None:
            if native:
                version = '1.2.3'
            else:
                version = '1.2.3-4ubuntu5'

        debian = os.path.join(self.tmpdir.name, 'debian')

        if os.path.exists(debian):
            shutil.rmtree(debian)

        os.makedirs(os.path.join(debian, 'source'))

        if format:
            with open(
                os.path.join(debian, 'source', 'format'),
                'w',
            ) as writer:
                writer.write(format + '\n')

        with open(
            os.path.join(debian, 'changelog'),
            'w',
        ) as writer:
            writer.write(
                '{} ({}) whatever; urgency=low\n'.format(
                    source,
                    version,
                )
            )
            writer.write('\n')
            writer.write('  * Changelog entry\n')
            writer.write('\n')
            writer.write(
                ' -- Maintainer <me@example.com>  '
                'Mon, 27 Jan 2020 01:02:03 +0400\n'
            )

            for v in old_versions:
                writer.write(
                    '{} ({}) whatever; urgency=low\n'.format(
                        source,
                        v,
                    )
                )
                writer.write('\n')
                writer.write('  * Changelog entry\n')
                writer.write('\n')
                writer.write(
                    ' -- Maintainer <me@example.com>  '
                    'Mon, 27 Jan 2020 01:02:03 +0400\n'
                )

    def test_no_tags(self):
        self.populate_tmpdir()

        for upstream in False, True:
            with self.subTest(upstream=upstream):
                text = subprocess.check_output(self.deb_git_version_gen + [
                    '--json',
                    '--upstream' if upstream else '--packaging-only',
                ], cwd=self.tmpdir.name).decode('utf-8')
                obj = json.loads(text)
                self.assertEqual(obj['changelog_version'], '1.2.3-4ubuntu5')
                self.assertEqual(obj['commit'], MOCK_COMMIT)
                self.assertEqual(obj['counter'], MOCK_COMMIT_COUNT)
                self.assertIs(obj['is_native'], False)
                self.assertIs(obj['is_upstream'], upstream)

                if upstream:
                    expected_version = '1.2.3'
                    expected_tail = '-0~snapshot'
                else:
                    expected_version = '1.2.3-4ubuntu5'
                    expected_tail = ''

                self.assertEqual(
                    obj['snapshot_version'],
                    '{}~~+{}+g{}{}'.format(
                        expected_version,
                        obj['counter'],
                        obj['commit'],
                        expected_tail,
                    ),
                )

    def test_no_tags_native(self):
        self.populate_tmpdir(native=True)

        for upstream in False, True:
            with self.subTest(upstream=upstream):
                text = subprocess.check_output(self.deb_git_version_gen + [
                    '--json',
                    '--upstream' if upstream else '--packaging-only',
                ], cwd=self.tmpdir.name).decode('utf-8')
                obj = json.loads(text)
                self.assertEqual(obj['changelog_version'], '1.2.3')
                self.assertEqual(obj['commit'], MOCK_COMMIT)
                self.assertEqual(obj['counter'], MOCK_COMMIT_COUNT)
                self.assertIs(obj['is_native'], True)
                # TODO: is_upstream is always False here, is that appropriate?
                self.assertEqual(
                    obj['snapshot_version'],
                    '{}~~+{}+g{}'.format(
                        obj['changelog_version'],
                        obj['counter'],
                        obj['commit'],
                    ),
                )

                text = subprocess.check_output(self.deb_git_version_gen + [
                    '--upstream' if upstream else '--packaging-only',
                ], cwd=self.tmpdir.name).decode('utf-8')
                self.assertEqual(text, obj['snapshot_version'] + '\n')

    def test_debian_revisions_since_latest_tag(self):
        self.populate_tmpdir(
            old_versions=(
                '1.2.3-4',
                '1.2.3-1',
                '1.2.2-1',
                '1.2.0-1',
                '1.0-1',
            ),
        )

        env = dict(os.environ)
        env['MOCK_GIT_TAGS'] = '\n'.join([
            json.dumps(
                dict(age=5, tag='debian/1.2.3-1', has_upstream_changes=False),
            ),
            json.dumps(dict(age=10, tag='1.2.3', has_upstream_changes=False)),
            json.dumps(dict(age=15, tag='debian/1.2.0-1')),
            json.dumps(dict(age=20, tag='1.2.0')),
        ])

        with self.subTest(upstream=False):
            text = subprocess.check_output(self.deb_git_version_gen + [
                '--json',
                '--packaging-only',
            ], cwd=self.tmpdir.name, env=env).decode('utf-8')
            obj = json.loads(text)
            self.assertEqual(obj['changelog_version'], '1.2.3-4ubuntu5')
            self.assertEqual(obj['commit'], MOCK_COMMIT)
            self.assertEqual(obj['counter'], 5)
            self.assertIs(obj['is_native'], False)
            self.assertIs(obj['is_upstream'], False)
            self.assertEqual(
                obj['snapshot_version'],
                # Read as: a prerelease of 1.2.3-4ubuntu5, 5 commits after
                # revision [1.2.3-]1
                '1.2.3-4ubuntu5~1+5+g1a2b3c4',
            )

        with self.subTest(upstream=True):
            text = subprocess.check_output(self.deb_git_version_gen + [
                '--json',
                '--upstream',
            ], cwd=self.tmpdir.name, env=env).decode('utf-8')
            obj = json.loads(text)
            self.assertEqual(obj['changelog_version'], '1.2.3-4ubuntu5')
            self.assertEqual(obj['commit'], MOCK_COMMIT)
            self.assertEqual(obj['counter'], 10)
            self.assertIs(obj['is_native'], False)
            self.assertIs(obj['is_upstream'], True)
            self.assertEqual(
                obj['snapshot_version'],
                # Read as: 10 commits after 1.2.3
                '1.2.3+10+g1a2b3c4-0~snapshot',
            )

        with self.subTest(upstream='auto-not-triggered'):
            text = subprocess.check_output(self.deb_git_version_gen + [
                '--json',
                '--auto-upstream',
            ], cwd=self.tmpdir.name, env=env).decode('utf-8')
            obj = json.loads(text)
            self.assertEqual(obj['changelog_version'], '1.2.3-4ubuntu5')
            self.assertEqual(obj['commit'], MOCK_COMMIT)
            self.assertEqual(obj['counter'], 5)
            self.assertIs(obj['is_native'], False)
            self.assertIs(obj['is_upstream'], False)
            self.assertEqual(
                obj['snapshot_version'],
                # Read as: a prerelease of 1.2.3-4ubuntu5, 5 commits after
                # revision [1.2.3-]1
                '1.2.3-4ubuntu5~1+5+g1a2b3c4',
            )

        env = dict(os.environ)
        env['MOCK_GIT_TAGS'] = '\n'.join([
            json.dumps(
                dict(age=5, tag='debian/1.2.3-1', has_upstream_changes=False),
            ),
            json.dumps(dict(age=10, tag='1.2.3')),
            json.dumps(dict(age=15, tag='debian/1.2.0-1')),
            json.dumps(dict(age=20, tag='1.2.0')),
        ])

        with self.subTest(upstream='auto-triggered'):
            text = subprocess.check_output(self.deb_git_version_gen + [
                '--json',
                '--auto-upstream',
            ], cwd=self.tmpdir.name, env=env).decode('utf-8')
            obj = json.loads(text)
            self.assertEqual(obj['changelog_version'], '1.2.3-4ubuntu5')
            self.assertEqual(obj['commit'], MOCK_COMMIT)
            self.assertEqual(obj['counter'], 10)
            self.assertIs(obj['is_native'], False)
            self.assertIs(obj['is_upstream'], True)
            self.assertEqual(
                obj['snapshot_version'],
                # Read as: 10 commits after 1.2.3
                '1.2.3+10+g1a2b3c4-0~snapshot',
            )

        with self.subTest('cannot-release'):
            with self.assertRaises(subprocess.CalledProcessError):
                subprocess.check_output(self.deb_git_version_gen + [
                    '--json',
                    '--release',
                    '--auto-upstream',
                ], cwd=self.tmpdir.name, env=env)

    def test_upstream_changes_since_latest_tag(self):
        self.populate_tmpdir(
            old_versions=(
                '1.2.3-4',
                '1.2.3-1',
                '1.2.2-1',
                '1.2.0-1',
                '1.0-1',
            ),
        )

        env = dict(os.environ)
        env['MOCK_GIT_TAGS'] = '\n'.join([
            json.dumps(dict(age=5, tag='debian/1.2.3-1')),
            json.dumps(dict(age=10, tag='1.2.3')),
            json.dumps(dict(age=15, tag='debian/1.2.0-1')),
            json.dumps(dict(age=20, tag='1.2.0')),
        ])

        with self.subTest(upstream=False):
            text = subprocess.check_output(self.deb_git_version_gen + [
                '--json',
                '--packaging-only',
            ], cwd=self.tmpdir.name, env=env).decode('utf-8')
            obj = json.loads(text)
            self.assertEqual(obj['changelog_version'], '1.2.3-4ubuntu5')
            self.assertEqual(obj['commit'], MOCK_COMMIT)
            self.assertEqual(obj['counter'], 5)
            self.assertIs(obj['is_native'], False)
            self.assertIs(obj['is_upstream'], False)
            self.assertEqual(
                obj['snapshot_version'],
                # Read as: a prerelease of 1.2.3-4ubuntu5, 5 commits after
                # revision [1.2.3-]1
                '1.2.3-4ubuntu5~1+5+g1a2b3c4',
            )

        with self.subTest(upstream=True):
            text = subprocess.check_output(self.deb_git_version_gen + [
                '--json',
                '--upstream',
            ], cwd=self.tmpdir.name, env=env).decode('utf-8')
            obj = json.loads(text)
            self.assertEqual(obj['changelog_version'], '1.2.3-4ubuntu5')
            self.assertEqual(obj['commit'], MOCK_COMMIT)
            self.assertEqual(obj['counter'], 10)
            self.assertIs(obj['is_native'], False)
            self.assertIs(obj['is_upstream'], True)
            self.assertEqual(
                obj['snapshot_version'],
                # Read as: 10 commits after 1.2.3
                '1.2.3+10+g1a2b3c4-0~snapshot',
            )

        with self.subTest(upstream='auto-triggered'):
            text = subprocess.check_output(self.deb_git_version_gen + [
                '--json',
                '--auto-upstream',
            ], cwd=self.tmpdir.name, env=env).decode('utf-8')
            obj = json.loads(text)
            self.assertEqual(obj['changelog_version'], '1.2.3-4ubuntu5')
            self.assertEqual(obj['commit'], MOCK_COMMIT)
            self.assertEqual(obj['counter'], 10)
            self.assertIs(obj['is_native'], False)
            self.assertIs(obj['is_upstream'], True)
            self.assertEqual(
                obj['snapshot_version'],
                # Read as: 10 commits after 1.2.3
                '1.2.3+10+g1a2b3c4-0~snapshot',
            )

        with self.subTest('cannot-release'):
            with self.assertRaises(subprocess.CalledProcessError):
                subprocess.check_output(self.deb_git_version_gen + [
                    '--json',
                    '--release',
                    '--auto-upstream',
                ], cwd=self.tmpdir.name, env=env)

    def test_upstream_prerelease(self):
        self.populate_tmpdir(
            old_versions=(
                '1.2.3-4',
                '1.2.3-1',
                '1.2.2-1',
                '1.2.0-1',
                '1.0-1',
            ),
            version='1.2.4-1',
        )

        env = dict(os.environ)
        env['MOCK_GIT_TAGS'] = '\n'.join([
            json.dumps(dict(age=5, tag='debian/1.2.3-1')),
            json.dumps(dict(age=10, tag='1.2.3')),
            json.dumps(dict(age=15, tag='debian/1.2.0-1')),
            json.dumps(dict(age=20, tag='1.2.0')),
        ])

        with self.subTest(upstream=True):
            text = subprocess.check_output(self.deb_git_version_gen + [
                '--json',
                '--upstream',
            ], cwd=self.tmpdir.name, env=env).decode('utf-8')
            obj = json.loads(text)
            self.assertEqual(obj['changelog_version'], '1.2.4-1')
            self.assertEqual(obj['commit'], MOCK_COMMIT)
            self.assertEqual(obj['counter'], 10)
            self.assertIs(obj['is_native'], False)
            self.assertIs(obj['is_upstream'], True)
            self.assertEqual(
                obj['snapshot_version'],
                # Read as: a prerelease of 1.2.4, 10 commits after 1.2.3
                '1.2.4~1.2.3+10+g1a2b3c4-0~snapshot',
            )

        with self.subTest('cannot-release'):
            with self.assertRaises(subprocess.CalledProcessError):
                subprocess.check_output(self.deb_git_version_gen + [
                    '--json',
                    '--release',
                    '--upstream',
                ], cwd=self.tmpdir.name, env=env)

    def test_non_native_with_epoch(self):
        self.populate_tmpdir(
            version='1:1.2-1',
            old_versions=(
                '1:1.1-1',
                '1:1.0-1',
                '20200221',
            ),
        )

        for (tags, commits_since) in product(
            (
                'debian-only',
                'debian+numeric',
                'debian+v',
                'debian+numeric+epoch',
                'debian+v+epoch',
            ),
            (0, 5),
        ):
            env = dict(os.environ)

            if '+numeric' in tags:
                maybe_v = ''
            else:
                maybe_v = 'v'

            if '+epoch' in tags:
                maybe_epoch = '1%'
            else:
                maybe_epoch = ''

            env['MOCK_GIT_TAGS'] = '\n'.join([
                json.dumps(dict(age=commits_since, tag='debian/1%1.2-1')),
                json.dumps(
                    dict(
                        age=commits_since * 2,
                        tag=maybe_v + maybe_epoch + '1.2',
                    ),
                ),
                json.dumps(dict(age=15, tag='debian/1%1.1')),
                json.dumps(dict(age=20, tag=maybe_v + maybe_epoch + '1.1')),
                json.dumps(dict(age=25, tag='debian/1%1.0')),
                json.dumps(dict(age=30, tag=maybe_v + maybe_epoch + '1.0')),
                json.dumps(dict(age=35, tag='debian/20200221')),
                json.dumps(dict(age=40, tag=maybe_v + '20200221')),
            ])

            if tags == 'debian-only':
                env['MOCK_GIT_TAGS'] = '\n'.join([
                    json.dumps(dict(age=commits_since, tag='debian/1%1.2-1')),
                    json.dumps(dict(age=15, tag='debian/1%1.1')),
                    json.dumps(dict(age=25, tag='debian/1%1.0')),
                    json.dumps(dict(age=35, tag='debian/20200221')),
                ])

            with self.subTest(
                tags=tags,
                upstream=False,
                commits_since=commits_since,
            ):
                text = subprocess.check_output(self.deb_git_version_gen + [
                    '--json',
                    '--packaging-only',
                ], cwd=self.tmpdir.name, env=env).decode('utf-8')
                obj = json.loads(text)
                self.assertEqual(obj['changelog_version'], '1:1.2-1')
                self.assertEqual(obj['commit'], MOCK_COMMIT)
                self.assertEqual(obj['counter'], commits_since)
                self.assertIs(obj['is_native'], False)
                self.assertIs(obj['is_upstream'], False)
                self.assertEqual(
                    obj['snapshot_version'],
                    '1:1.2-1+{}+g{}'.format(commits_since, MOCK_COMMIT),
                )

            with self.subTest(
                tags=tags,
                upstream=True,
                commits_since=commits_since,
            ):
                text = subprocess.check_output(self.deb_git_version_gen + [
                    '--json',
                    '--upstream',
                ], cwd=self.tmpdir.name, env=env).decode('utf-8')
                obj = json.loads(text)
                self.assertEqual(obj['changelog_version'], '1:1.2-1')
                self.assertEqual(obj['commit'], MOCK_COMMIT)
                self.assertIs(obj['is_native'], False)
                self.assertIs(obj['is_upstream'], True)
                if tags == 'debian-only':
                    self.assertEqual(obj['counter'], 123)
                    self.assertEqual(
                        obj['snapshot_version'],
                        '1:1.2~~+123+g' + MOCK_COMMIT + '-0~snapshot',
                    )
                else:
                    self.assertEqual(obj['counter'], commits_since * 2)
                    self.assertEqual(
                        obj['snapshot_version'],
                        '1:1.2+{}+g{}-0~snapshot'.format(
                            commits_since * 2,
                            MOCK_COMMIT,
                        ),
                    )

            if commits_since == 0:
                with self.subTest('can-release', tags=tags):
                    subprocess.check_output(self.deb_git_version_gen + [
                        '--json',
                        '--release',
                        '--packaging-only',
                    ], cwd=self.tmpdir.name, env=env)

                    if tags != 'debian-only':
                        subprocess.check_output(self.deb_git_version_gen + [
                            '--json',
                            '--release',
                            '--upstream',
                        ], cwd=self.tmpdir.name, env=env)
            else:
                with self.subTest('cannot-release', tags=tags):
                    with self.assertRaises(subprocess.CalledProcessError):
                        subprocess.check_output(self.deb_git_version_gen + [
                            '--json',
                            '--release',
                            '--packaging-only',
                        ], cwd=self.tmpdir.name, env=env)

                    with self.assertRaises(subprocess.CalledProcessError):
                        subprocess.check_output(self.deb_git_version_gen + [
                            '--json',
                            '--release',
                            '--upstream',
                        ], cwd=self.tmpdir.name, env=env)

        for (tags, commits_since) in product(
            (
                'numeric',
                'v',
                'numeric+epoch',
                'v+epoch',
            ),
            (0, 10),
        ):
            env = dict(os.environ)

            if 'numeric' in tags:
                maybe_v = ''
            else:
                maybe_v = 'v'

            if 'epoch' in tags:
                maybe_epoch = '1%'
            else:
                maybe_epoch = ''

            env['MOCK_GIT_TAGS'] = '\n'.join([
                json.dumps(
                    dict(age=commits_since, tag=maybe_v + maybe_epoch + '1.2'),
                ),
                json.dumps(dict(age=20, tag=maybe_v + maybe_epoch + '1.1')),
                json.dumps(dict(age=30, tag=maybe_v + maybe_epoch + '1.0')),
                json.dumps(dict(age=40, tag=maybe_v + '20200221')),
            ])

            with self.subTest(tags=tags, upstream=True):
                text = subprocess.check_output(self.deb_git_version_gen + [
                    '--json',
                    '--upstream',
                ], cwd=self.tmpdir.name, env=env).decode('utf-8')
                obj = json.loads(text)
                self.assertEqual(obj['changelog_version'], '1:1.2-1')
                self.assertEqual(obj['commit'], MOCK_COMMIT)
                self.assertEqual(obj['counter'], commits_since)
                self.assertIs(obj['is_native'], False)
                self.assertIs(obj['is_upstream'], True)
                self.assertEqual(
                    obj['snapshot_version'],
                    '1:1.2+{}+g{}-0~snapshot'.format(
                        commits_since,
                        MOCK_COMMIT,
                    )
                )

            if commits_since == 0:
                with self.subTest('can-release', tags=tags):
                    if tags != 'debian-only':
                        subprocess.check_output(self.deb_git_version_gen + [
                            '--json',
                            '--release',
                            '--upstream',
                        ], cwd=self.tmpdir.name, env=env)
            else:
                with self.subTest('cannot-release', tags=tags):
                    with self.assertRaises(subprocess.CalledProcessError):
                        subprocess.check_output(self.deb_git_version_gen + [
                            '--json',
                            '--release',
                            '--upstream',
                        ], cwd=self.tmpdir.name, env=env)

    def test_native_with_epoch(self):
        self.populate_tmpdir(
            native=True,
            version='1:1.2',
            old_versions=(
                '1:1.1',
                '1:1.0',
                '20200221',
            ),
        )

        for upstream, tags, commits_since in product(
            (True, False),
            (
                'numeric',
                'v',
                'numeric+epoch',
                'v+epoch',
            ),
            (0, 10),
        ):
            env = dict(os.environ)

            if 'numeric' in tags:
                maybe_v = ''
            else:
                maybe_v = 'v'

            if 'epoch' in tags:
                maybe_epoch = '1%'
            else:
                maybe_epoch = ''

            tag_prefix = maybe_v + maybe_epoch

            env['MOCK_GIT_TAGS'] = '\n'.join([
                json.dumps(
                    dict(age=commits_since, tag=tag_prefix + '1.2'),
                ),
                json.dumps(
                    dict(age=commits_since + 10, tag=tag_prefix + '1.1'),
                ),
                json.dumps(
                    dict(age=commits_since + 20, tag=tag_prefix + '1.0'),
                ),
                json.dumps(
                    dict(age=commits_since + 30, tag=maybe_v + '20200221'),
                ),
            ])

            with self.subTest(
                upstream=upstream,
                tags=tags,
                commits_since=commits_since,
            ):
                text = subprocess.check_output(self.deb_git_version_gen + [
                    '--json',
                    '--upstream' if upstream else '--packaging-only',
                ], cwd=self.tmpdir.name, env=env).decode('utf-8')
                obj = json.loads(text)
                self.assertEqual(obj['changelog_version'], '1:1.2')
                self.assertEqual(obj['commit'], MOCK_COMMIT)
                self.assertEqual(obj['counter'], commits_since)
                self.assertIs(obj['is_native'], True)
                self.assertIs(obj['is_upstream'], False)
                self.assertEqual(
                    obj['snapshot_version'],
                    '1:1.2+{}+g{}'.format(commits_since, MOCK_COMMIT),
                )

                if commits_since == 0:
                    subprocess.check_output(self.deb_git_version_gen + [
                        '--json',
                        '--release',
                        '--upstream' if upstream else '--packaging-only',
                    ], cwd=self.tmpdir.name, env=env)
                else:
                    with self.assertRaises(subprocess.CalledProcessError):
                        subprocess.check_output(self.deb_git_version_gen + [
                            '--json',
                            '--release',
                            '--upstream' if upstream else '--packaging-only',
                        ], cwd=self.tmpdir.name, env=env)

    def test_guess_release(self):
        for native in False, True:
            if native:
                revision = ''
                snapshot_revision = ''
            else:
                revision = '-1'
                snapshot_revision = '-0~snapshot'

            self.populate_tmpdir(
                native=native,
                old_versions=(
                    '1.2.3' + revision,
                    '1.2.0' + revision,
                    '1.0' + revision,
                ),
                version='1.2.3' + revision,
            )

            for tag_prefix in ('', 'v'):
                for since_tag in (0, 5):
                    env = dict(os.environ)
                    env['MOCK_GIT_TAGS'] = '\n'.join([
                        json.dumps(
                            dict(age=since_tag, tag=tag_prefix + '1.2.3'),
                        ),
                        json.dumps(
                            dict(age=since_tag + 10, tag=tag_prefix + '1.2.0'),
                        ),
                        json.dumps(
                            dict(age=since_tag + 20, tag=tag_prefix + '1.0'),
                        ),
                    ])

                    with self.subTest(
                        native=native,
                        since_tag=since_tag,
                        tag_prefix=tag_prefix,
                    ):
                        text = subprocess.check_output(
                            self.deb_git_version_gen + [
                                '--json',
                                '--upstream',
                                '--guess-release',
                            ],
                            cwd=self.tmpdir.name,
                            env=env
                        ).decode('utf-8')
                        obj = json.loads(text)
                        self.assertEqual(
                            obj['changelog_version'],
                            '1.2.3' + revision,
                        )
                        self.assertEqual(obj['commit'], MOCK_COMMIT)
                        self.assertEqual(obj['counter'], since_tag)
                        self.assertIs(obj['is_native'], native)
                        self.assertIs(obj['is_upstream'], not native)

                        if since_tag == 0:
                            self.assertEqual(
                                obj['snapshot_version'],
                                '1.2.3' + revision,
                            )
                        else:
                            self.assertEqual(
                                obj['snapshot_version'],
                                '1.2.3+{}+g{}{}'.format(
                                    obj['counter'],
                                    obj['commit'],
                                    snapshot_revision,
                                ),
                            )

    def test_packaging_prerelease(self):
        self.populate_tmpdir(
            old_versions=(
                '1.2.3-4',
                '1.2.3-1',
                '1.2.2-1',
                '1.2.0-1',
                '1.0-1',
            ),
            version='1.2.3-5',
        )

        env = dict(os.environ)
        env['MOCK_GIT_TAGS'] = '\n'.join([
            json.dumps(
                dict(age=5, tag='debian/1.2.3-4', has_upstream_changes=False),
            ),
        ])

        with self.subTest(upstream=True):
            text = subprocess.check_output(self.deb_git_version_gen + [
                '--json',
                '--packaging-only',
            ], cwd=self.tmpdir.name, env=env).decode('utf-8')
            obj = json.loads(text)
            self.assertEqual(obj['changelog_version'], '1.2.3-5')
            self.assertEqual(obj['commit'], MOCK_COMMIT)
            self.assertEqual(obj['counter'], 5)
            self.assertIs(obj['is_native'], False)
            self.assertIs(obj['is_upstream'], False)
            self.assertEqual(
                obj['snapshot_version'],
                '1.2.3-5~4+5+g1a2b3c4',
            )

    def test_non_native_becomes_native(self):
        self.populate_tmpdir(
            native=True,
            old_versions=(
                '0.1706.0-0co4',
                '0.1706.0-0co2',
                '0.1706.0-0co3',
                '0.1706.0-0co1',
                '0.1703.0-0co1',
            ),
            version='0.2020.1',
        )

        env = dict(os.environ)
        env['MOCK_GIT_TAGS'] = '\n'.join([
            json.dumps(dict(age=3, tag='apertis/0.1706.0-0co4')),
            json.dumps(dict(age=13, tag='apertis/0.1706.0-0co3')),
            json.dumps(dict(age=23, tag='apertis/0.1706.0-0co2')),
            json.dumps(dict(age=33, tag='apertis/0.1706.0-0co1')),
            json.dumps(dict(age=43, tag='apertis/0.1703.0-0co1')),
        ])

        with self.subTest(upstream=True):
            text = subprocess.check_output(self.deb_git_version_gen + [
                '--json',
                '--upstream',
            ], cwd=self.tmpdir.name, env=env).decode('utf-8')
            obj = json.loads(text)
            self.assertEqual(obj['changelog_version'], '0.2020.1')
            self.assertEqual(obj['commit'], MOCK_COMMIT)
            self.assertEqual(obj['counter'], 3)
            self.assertIs(obj['is_native'], True)
            self.assertIs(obj['is_upstream'], False)
            self.assertEqual(
                obj['snapshot_version'],
                '0.2020.1~0.1706.0+0co4+3+g' + MOCK_COMMIT,
            )

    def test_vendor_tag_in_different_place(self):
        self.populate_tmpdir(
            version='1.2-3',
        )

        with self.subTest('development'):
            env = dict(os.environ)
            env['MOCK_GIT_TAGS'] = '\n'.join([
                json.dumps(dict(age=3, tag='apertis/1.2-3')),
                json.dumps(dict(age=10, tag='debian/1.2-3')),
            ])

            text = subprocess.check_output(self.deb_git_version_gen + [
                '--json',
                '--packaging-only',
            ], cwd=self.tmpdir.name, env=env).decode('utf-8')
            obj = json.loads(text)
            self.assertEqual(obj['changelog_version'], '1.2-3')
            self.assertEqual(obj['commit'], MOCK_COMMIT)
            self.assertEqual(obj['counter'], 3)
            self.assertIs(obj['is_native'], False)
            self.assertIs(obj['is_upstream'], False)
            self.assertEqual(
                obj['snapshot_version'],
                '1.2-3+3+g' + MOCK_COMMIT,
            )

        with self.subTest('other vendor'):
            env = dict(os.environ)
            env['MOCK_GIT_TAGS'] = '\n'.join([
                json.dumps(dict(age=3, tag='apertis/1.2-3')),
                json.dumps(dict(age=10, tag='debian/1.2-3')),
            ])

            text = subprocess.check_output(self.deb_git_version_gen + [
                '--json',
                '--packaging-only',
                '--vendor=debian',
            ], cwd=self.tmpdir.name, env=env).decode('utf-8')
            obj = json.loads(text)
            self.assertEqual(obj['changelog_version'], '1.2-3')
            self.assertEqual(obj['commit'], MOCK_COMMIT)
            self.assertEqual(obj['counter'], 10)
            self.assertIs(obj['is_native'], False)
            self.assertIs(obj['is_upstream'], False)
            self.assertEqual(
                obj['snapshot_version'],
                '1.2-3+10+g' + MOCK_COMMIT,
            )

        with self.subTest('stage release'):
            env = dict(os.environ)
            env['MOCK_GIT_TAGS'] = '\n'.join([
                json.dumps(dict(age=0, tag='apertis/1.2-3')),
                json.dumps(dict(age=10, tag='debian/1.2-3')),
            ])

            text = subprocess.check_output(self.deb_git_version_gen + [
                '--json',
                '--packaging-only',
                '--release',
            ], cwd=self.tmpdir.name, env=env).decode('utf-8')
            obj = json.loads(text, encoding='utf-8')
            self.assertEqual(obj['changelog_version'], '1.2-3')
            self.assertEqual(obj['commit'], MOCK_COMMIT)
            self.assertEqual(obj['counter'], 0)
            self.assertIs(obj['is_native'], False)
            self.assertIs(obj['is_upstream'], False)
            self.assertEqual(
                obj['snapshot_version'],
                '1.2-3',
            )

        with self.subTest('release'):
            env = dict(os.environ)
            env['MOCK_GIT_TAGS'] = '\n'.join([
                json.dumps(dict(age=0, tag='apertis/1.2-3')),
                json.dumps(dict(age=10, tag='debian/1.2-3')),
            ])

            text = subprocess.check_output(self.deb_git_version_gen + [
                '--json',
                '--packaging-only',
                '--release',
            ], cwd=self.tmpdir.name, env=env).decode('utf-8')
            obj = json.loads(text, encoding='utf-8')
            self.assertEqual(obj['changelog_version'], '1.2-3')
            self.assertEqual(obj['commit'], MOCK_COMMIT)
            self.assertEqual(obj['counter'], 0)
            self.assertIs(obj['is_native'], False)
            self.assertIs(obj['is_upstream'], False)
            self.assertEqual(
                obj['snapshot_version'],
                '1.2-3',
            )

    def tearDown(self) -> None:
        self.tmpdir.cleanup()


def main():
    # type: (...) -> None
    if TAPTestRunner is not None:
        runner = TAPTestRunner()
        runner.set_stream(True)
        unittest.main(testRunner=runner)
    else:
        # You thought pycotap was a minimal TAP implementation?
        print('1..1')
        program = unittest.main(exit=False)
        if program.result.wasSuccessful():
            print(
                'ok 1 - %r (tap module not available)'
                % program.result
            )
        else:
            print(
                'not ok 1 - %r (tap module not available)'
                % program.result
            )


if __name__ == '__main__':
    main()

# vi: set sw=4 sts=4 et:
