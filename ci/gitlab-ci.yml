# Copyright © 2019 Collabora Ltd.
#
# SPDX-License-Identifier: MIT
#
# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
# CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
# SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

stages:
    - build
    - test
    - analyze

before_script:
    - |
        set -eux
        if [ -n "${DEPS-}" ]; then
            apt-get -y update
            apt-get install -y --no-install-recommends ${DEPS}
        fi

.build:
    stage: build
    image: ${BUILD_IMAGE}
    variables:
        DEPS: >-
          dpkg-dev
          git
          make
          mypy
          perl
          pigz
          pycodestyle
          pyflakes3
          python3-debian
          python3-tap
          shellcheck
    # Assume Gitlab-CI uses bash, not sh (bash is the default)
    script: |
        set -eux
        set -o pipefail
        make check 2>&1 | tee qa.log
        perl -ne 'print if s{^deb }{deb-src }' \
            < /etc/apt/sources.list \
            >> /etc/apt/sources.list.d/deb-src.list
        apt-get update
        set --
        if [ -z "${CI_COMMIT_TAG-}" ]; then
            set -- "$@" --set-suffix="${CI_COMMIT_REF_SLUG}"
        fi
        ./build-standalone.sh --apt "$@"
    # Save qa.log for analysis
    artifacts:
        paths:
            - qa.log

    artifacts:
        paths:
            - deb-build-snapshot-standalone-*.tar.gz

.test-unit:
    stage: test
    image: ${BUILD_IMAGE}
    variables:
        DEPS: >-
          dpkg-dev
          git
          make
          perl
          python3-debian
          python3-tap
    script: |
        set -eux
        make check

.test-standalone:
    stage: test
    image: ${BUILD_IMAGE}
    variables:
        DEPS: python3
        GIT_STRATEGY: none
    script: |
        set -eux

        unpacked="$(mktemp -d --tmpdir="$(pwd)")"
        cd "${unpacked}"
        tar -xvf ../deb-build-snapshot-standalone-*.tar.gz
        cd deb-build-snapshot-standalone-*/
        ./deb-build-snapshot --help
        ./deb-git-version-gen --help

build:buster:
    extends: .build
    variables:
        BUILD_IMAGE: 'debian:buster-slim'

test:unit:stretch:
    extends: .test-unit
    variables:
        BUILD_IMAGE: 'debian:stretch-slim'
        DEPS: >-
          dpkg-dev
          git
          make
          perl
          python3-debian

test:unit:buster:
    extends: .test-unit
    variables:
        BUILD_IMAGE: 'debian:buster-slim'

test:unit:jessie:
    extends: .test-unit
    variables:
        BUILD_IMAGE: 'debian:jessie-slim'
        DEPS: >-
          dpkg-dev
          git
          make
          perl
          python3-debian

test:standalone:stretch:
    extends: .test-standalone
    variables:
        BUILD_IMAGE: 'debian:stretch-slim'

test:standalone:buster:
    extends: .test-standalone
    variables:
        BUILD_IMAGE: 'debian:buster-slim'

test:standalone:jessie:
    extends: .test-standalone
    variables:
        BUILD_IMAGE: 'debian:jessie-slim'

todo:
    stage: analyze
    variables:
        GIT_STRATEGY: none
    # Succeed if grep fails (no failing or expected-failure tests),
    # fail if grep succeeds
    script: |
        ! grep -B10 '^not ok\b' qa.log
    # Failure is just a warning
    allow_failure: true
