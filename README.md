deb-build-snapshot -- build Debian package snapshots from git
=============================================================

<!-- This document:
Copyright 2017-2020 Collabora Ltd.
SPDX-License-Identifier: MIT
-->

**deb-git-version-gen** manufactures synthetic version numbers for Debian
packages based on `git describe`, so developers can track which build
they have ended up using.

**deb-build-snapshot** turns packages' source code in git into tested
(and hopefully working) source .dsc and optionally binary .deb snapshots.
It can also be used to do releases in a CI system or other controlled
environment. Optionally, it can do the build remotely and/or in a
`schroot`.

Assumptions:

* The current working directory is the root of a git repository
* The current branch has upstream sources and a `debian/` directory,
  or can arrange to contain those by merging a branch
* Debian packaging tags look like `debian/1.2.3-4`,
  or `vendor/1.2.3-4vendor5` for Debian derivatives such as Ubuntu

Additional assumptions with `--upstream` or `--auto-upstream`:

* Upstream tags look like v1.2.3
* The upstream project uses either Autotools or Meson
* If Autotools, `NOCONFIGURE=1 ./autogen.sh` and `./configure` use
  reasonable build options by default
* If Meson, `ninja dist` uses reasonable build options by default
* We are either building locally, or on a machine (the *builder*) that
  can be accessed via `ssh` and `rsync`
* `mktemp` uses a location with plenty of space, both locally and on the
  builder
* Parallel builds work correctly, or are explicitly disabled with
  GNU make `.NOTPARALLEL` or similar

There are various options, but the most important are:

* `--packaging`, or absence of `--upstream`:
  Asssume we represent a Debian derivative that is packaging this
  software. Take an existing `orig.tar.*` tarball, or reconstitute
  one with pristine-tar(1); build Debian packages from that, using
  a manufactured Debian revision.

* `--upstream`:
  Assume we represent the upstream developer, responsible for making
  new upstream releases of this software (which can either be a native
  or non-native package). Do an upstream build - run Autotools `make dist`
  or `make distcheck`, or Meson `ninja dist` - then rename the resulting
  tarball to have a manufactured snapshot version, and build Debian
  packages from that. If it is non-native, use Debian revision
  `0~snapshot`.

Vendored dependencies
---------------------

These scripts normally use and require a system copy of python3-debian
and its dependencies.

If you need a version of these scripts that can run in old operating
systems, try running `./build-vendored.sh` on a Debian 10 system.
The result still requires Python 3 version 3.4 or later, `devscripts`
(with its mandatory dependencies), `git` and `rsync`, but bundles a copy
of its required Python modules.

History
-------

This script originated in the Apertis open source automotive OS,
to automate what smcv was doing anyway, and accidentally became part
of its continuous integration infrastructure. However, it is designed
to avoid Apertis-specific assumptions, and has already been useful in
other projects.

Relatives
---------

If you like this sort of thing, you might also like
<https://salsa.debian.org/smcv/vectis>, which has not yet subsumed the
functionality of deb-build-snapshot but probably should.

TODO
----

The uses of `rsync` should maybe be replaced with `tar|ssh|tar`, so that
the uses of `ssh` can be replaced with a more generic "run this command,
but over there ->" interface like `autopkgtest-virt-qemu`'s `runcmd`.

Not TODO
--------

deb-build-snapshot was partially an experiment in writing something that
would normally be a shell script, using a programming language with fewer
sharp edges. smcv is happy with the result, so it will not be rewritten
in shell script.

When running deb-build-snapshot against an untrusted source tree with a
non-`localhost` `HOSTNAME` on the command-line, the goal is that nothing
in the source tree (configuration, build systems, whatever) can execute
arbitrary code on the system where deb-build-snapshot runs, only on the
system named by `HOSTNAME`.
